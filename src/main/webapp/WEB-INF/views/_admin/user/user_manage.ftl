<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TeaCMS-后台管理</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="/static/plugins/pace-0.7.5/pace.js"></script>
    <link href="/static/plugins/pace-0.7.5/themes/black/pace-theme-center-circle.css" rel="stylesheet" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <!--bootstrap-validator-->
    <link rel="stylesheet" href="/static/plugins/bootstrap-validator/css/bootstrapValidator.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/static/css/skins/my_all-skins.css">
    <!-- MyAdminLTE.css -->
    <link rel="stylesheet" href="/static/css/MyAdminLTE.css">
    <!-- Lobibox -->
    <link rel="stylesheet" href="/static/plugins/lobibox/css/lobibox.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="//cdn.bootcss.com/owl-carousel/1.32/owl.carousel.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed" >
<#import "/_admin/lib/sidebar_templet.ftl" as sidebar_templet>
<!-- Site wrapper -->
<div class="wrapper">
<#-- header -->
<#include "/_admin/header.ftl">
<#-- sidebar -->
<@sidebar_templet.sidebar "user-manage"></@sidebar_templet.sidebar>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                用户管理
                <small>User Manage</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content" id="app">
            <section class="">
                <!-- Default box -->
                <div class="col-md-5" style="padding: 0">
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="addTagOff" onclick="controlToggle()"><i id="rev" class='fa fa-minus-square'></i><i id="add" class='fa fa-plus-square'></i>添加</div>
                            <form id="addForm" method="post" action="#">
                                <h3 style="padding-top:20px;"><strong>添加用户</strong></h3>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="form-inline">用户名 （必填）</label>
                                        <input class="form-control" placeholder="" id="addName" name="userLogin" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-inline">昵称（文章作者）</label>
                                        <input class="form-control" placeholder="" id="addNiName" name="userNicename" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-inline">密码（必填）</label>
                                        <input class="form-control" placeholder="" id="addPass" name="userPass" autocomplete="off" type="password">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-inline">邮箱</label>
                                        <input class="form-control" placeholder="" id="addEmail" name="userEmail" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-inline">用户权限</label>
                                        <select class="form-control" id="addRight" name="userRight">
                                            <option value="0">普通用户</option>
                                            <option value="1">管理员</option>
                                        </select>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <button class="btn btn-success pull-right">添加</button>
                                    </div>
                                </div>
                            </form>
                            <div id="editTagOff" onclick="editToggle()"><i id="edit_rev" class='fa fa-minus-square'></i><i id="edit_add" class='fa fa-plus-square'></i>修改</div>
                            <form id="editForm" method="post" action="#">
                                <h3 style="padding-top:20px;"><strong>修改信息</strong></h3>
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="form-group"  style="display: none">
                                            <label class="form-inline">id</label>
                                            <input class="form-control" placeholder="" id="id" name="id" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-inline">用户名 （必填）</label>
                                            <input class="form-control" placeholder="" id="editLoginName" disabled="disabled"name="userLogin" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-inline">昵称（文章作者）</label>
                                            <input class="form-control" placeholder="" id="editNiceName"  disabled="disabled" name="userNicename" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-inline">密码（必填）</label>
                                            <input class="form-control" placeholder="" id="editPass" type="password" disabled="disabled" name="userPass" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-inline">邮箱</label>
                                            <input class="form-control" type="email" placeholder="" id="editEmail"  disabled="disabled" name="userEmail" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-inline">用户权限</label>
                                            <select class="form-control" id="editRight"  disabled="disabled" name="userRight">
                                                <option value="0">普通用户</option>
                                                <option value="1">管理员</option>
                                            </select>
                                        </div>
                                        <div class="top-bottom-10">
                                            <button  class="btn btn-primary">修改</button>
                                        </div>
                                    </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
    </div>
    <div class="col-md-7">
        <div class="box box-primary">

            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>用户名</th>
                        <th>昵称</th>
                        <th>邮箱</th>
                        <th>权限</th>
                        <th>操作1</th>
                        <th>操作2</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="el in dataTag.list" :id="el.id">
                        <td class="contentCenter">{{el.userLogin}}</td>
                        <td class="contentCenter">{{el.userNicename}}</td>
                        <td class="contentCenter">{{el.userEmail}}</td>
                        <td class="contentCenter" v-if="el.userRight==1">管理员</td>
                        <td class="contentCenter" v-else="el.userRight==0">用户</td>
                        <td><input class="btn btn-success btn-xs" v-on:click="editUser(el)" value="编辑" type="button"/></td>
                        <td><input class="btn btn-danger btn-xs" v-on:click="deleteUser(el.id)" value="删除" type="button"/></td>
                    </tr>
                    <tr v-show="">
                        <td colspan="9" align="center" style="vertical-align:middle;padding-top: 10px;padding-bottom: 10px;">暂无数据</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="box-footer clearfix">
            <ul class="pagination no-margin pull-right">
                <li v-if="!dataTag.isFirstPage"><a href="javscript:;" v-on:click="getAllTag(dataTag.prePage)">«</a></li>
                <li v-if="dataTag.isFirstPage" class="disabled"><a href="javscript:;">«</a></li>
                <li v-for="item in dataTag.navigatepageNums" :class="item == dataTag.pageNum? 'active':''"><a href="javascript:void(0);" v-text="item" v-on:click="getAllTag(item)"></a></li>
                <li v-if="!dataTag.isLastPage"><a href="javscript:;" v-on:click="getAllTag(dataTag.nextPage)">»</a><b></b></li>
                <li v-if="dataTag.isLastPage" class="disabled"><a href="javscript:;">»</a></li>
            </ul>
        </div>
        <div>
            <h4><strong>注：</strong><br></h4>

            <p>删除用户不会删除彻底删除数据</p>
        </div>
    </div>
    <div class="clearfix"></div>
    </section>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<#include "/_admin/footer.ftl">

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/plugins/jQueryForm/jquery.form.js"></script>
<!--vue-->
<script src="/static/plugins/vue/vue.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap.min.js"></script>
<!--bootstrap-validator-->
<script src="/static/plugins/bootstrap-validator/js/bootstrapValidator.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/js/demo.js"></script>
<script src="/static/plugins/lobibox/js/lobibox.min.js"></script>

<script src="/static/js/common.js"></script>

<script>
    //owl carousel
    $(document).ready(function() {
        $("#editForm").hide();
        $("#addForm").hide();
    });
    var addOff = true;
    var editOff = false;
    var clickEditIs = false;
    //custom select box
    $(function() {
        $("#add").hide();
        $("#rev").show();
        $("#edit_add").show();
        $("#edit_rev").hide();
        $("#editForm").hide();
        $("#addForm").show(200);
        addFormCheck1();
        editFormCheck1();
        getAllTag(0);
    });

    function deletMSG(type , msg) {
        Lobibox.notify(type, {
            size: 'mini',
            soundPath: '/static/plugins/lobibox/sounds/',
            position: 'center bottom',
            msg: msg
        });
    }

    var vm=new Vue({
        el:'#app',
        data: {
            dataTag: {}
        },
        methods: {
            getAllTag: function(id) {
                getAllTag(id);
            },
            editUser: function(userinfo) {
                showEditUser(userinfo);
            },
            deleteUser:function(id){
                showDeleteUser(id);
            },

        }
    });
    <!--查询所有用户信息-->
    function getAllTag(id) {
        $.get("/admin/finduserlist/"+id, function(result){
            // console.log(JSON.stringify(result));
            vm.dataTag = result.obj;
        });
    }
    <!--添加用户-->
    function addSubmit(){
        ajaxSubmitForm("/admin/user/add",$("#addForm").serialize(),successFn,errorFn);
    }
    function successFn(data) {
        deletMSG('success', '操作成功!');
    }
    function errorFn(data) {
        deletMSG('error', '网络错误，请刷新!');
    }
    <!--编辑用户-->
    function editSubmit(){
        ajaxSubmitForm("/admin/user/edit",$("#editForm").serialize(),successFn,errorFn);
        function sucess1(data) {
            editUserAfter();
            deletMSG('success', '操作成功!');
        }

    }
    function  editUserAfter() {
        $("#editForm").data('bootstrapValidator').destroy();
        $('#editForm').data('bootstrapValidator', null);
        editFormCheck1();
    }

    <!--删除用户-->

    function showDeleteUser(id){
        /*if (confirm("是否要删除这一行 ?") == false) {
            return;
        }*/
        ajaxSubmit("/admin/user/delete",JSON.stringify(id),sucess1,errorFn);
        function sucess1(data){
            $("#"+id).remove();
            deletMSG('success', '删除成功!');
        }

    }

    /* 点击编辑后调用的方法 */
    function showEditUser(obj){
        $("#id").val(obj.id);
        $("#editLoginName").val(obj.userLogin);
        $("#editNiceName").val(obj.userNicename);
        $("#editPass").val(obj.userPass);
        $("#editNiceName").val(obj.userNicename);
        $("#editEmail").val(obj.userEmail);
        $("#editRight").val(obj.userRight);
        $("#rev").hide(200);
        $("#add").show(200);
        $("#editForm").show(200);
        $("#addForm").hide(200);
        $("#editNiceName").removeAttr("disabled");
        $("#editPass").removeAttr("disabled");
        $("#editEmail").removeAttr("disabled");
        $("#editRight").removeAttr("disabled");


        clickEditIs = true;
        addOff = false;
    }
    /* 点击删除后调用删除的方法 */


    /* 显示与隐藏添加标签 */
    function editToggle(){
        if(editOff==true){
            $("#edit_rev").hide(200);
            $("#edit_add").show(200);
            $("#editForm").toggle(200);
            $("#addForm").hide(200);
            $("#rev").hide(200);
            $("#add").show(200);
            editOff = false;
        }else{
            $("#edit_rev").show(200);
            $("#edit_add").hide(200);
            $("#editForm").toggle(200);
            $("#addForm").hide(200);
            $("#rev").hide(200);
            $("#add").show(200);
            editOff = true;
        }

        if(!clickEditIs){
            $("#editLoginName").attr("disabled","disabled");
            $("#editNiceName").attr("disabled","disabled");
            $("#editPass").attr("disabled","disabled");
            $("#editEmail").attr("disabled","disabled");
            $("#editRight").attr("disabled","disabled");
        }
    }

</script>
</body>
</html>