package me.teacms.entity;

import java.util.List;

/**
 * Author: xiehuan
 * Email: 1487471733@qq.com
 * Date: 2017/2/22
 * Time: 17:49
 * Describe:针对用户进行分页管理的类
 */
public class UserListPage {

    private List<User> userList;

    private Integer pageSize;//每页显示的条数

    private Integer recordCount;//总共条数

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
    }
}
