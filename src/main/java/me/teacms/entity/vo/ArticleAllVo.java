package me.teacms.entity.vo;

import me.teacms.entity.Article;
import me.teacms.entity.Category;
import me.teacms.entity.Tag;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/24
 * Time: 17:34
 * Describe: 所有文章增强 Entity
 */
public class ArticleAllVo extends Article {

    private String userNicename;

    private List<Category> categories;

    private List<Tag> tags;

    public String getUserNicename() {
        return userNicename;
    }

    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
