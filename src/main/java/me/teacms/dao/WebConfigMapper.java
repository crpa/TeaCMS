package me.teacms.dao;

import me.teacms.entity.WebConfig;

public interface WebConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WebConfig record);

    int insertSelective(WebConfig record);

    WebConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WebConfig record);

    int updateByPrimaryKey(WebConfig record);
}