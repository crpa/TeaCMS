package me.teacms.dao;

import me.teacms.entity.User;
import me.teacms.entity.UserListPage;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //与系统分隔
    //更新用户状态为0
    Integer updateUserStatus(Integer id);

    List<User>  findAllUserInfo();

    //查询所有用户并且分页
    List<User> findAllUsers(UserListPage users);
    Integer findAllUserDataSize();

    /**
     * 查询用户密码是否正确
     * @param user
     * @return
     */
    User findUserPassIsCorrect(User user);

}