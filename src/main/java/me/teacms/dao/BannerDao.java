package me.teacms.dao;

import me.teacms.entity.vo.BannerVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/12
 * Time: 14:17
 * Describe:
 */
public interface BannerDao {

    /**
     * 查找所有Banner
     * @return
     */
    @Select("SELECT\n" +
            "	blogroll.id AS id,\n" +
            "	article.article_title AS articleTitle,\n" +
            "	blogroll.article_id AS articleId,\n" +
            "	blogroll.`order` AS `order`\n" +
            "FROM\n" +
            "	blogroll\n" +
            "LEFT JOIN article ON article.id = blogroll.article_id\n" +
            "WHERE\n" +
            "	article_state = 1\n" +
            "ORDER BY `order`")
    List<BannerVo> findAllBanner();

    /**
     * Banner 查找要添加所有Banner的文章
     * @return
     */
    @Select("SELECT\n" +
            "	article.id AS id,\n" +
            "	article.article_title AS articleTitle\n" +
            "FROM\n" +
            "	article\n" +
            "WHERE\n" +
            "	article_state = 1\n" +
            "ORDER BY\n" +
            "	update_date DESC")
    List<BannerVo> findAllBannerArticle();

    /**
     * 添加Banner
     * @return
     */
    @Insert("INSERT INTO `blogroll` (`article_id`, `order`)\n" +
            "VALUES\n" +
            "	(#{articleId}, #{order})")
    int addBanner(BannerVo bannerVo);

    /**
     * 更新 Banner
     * @param bannerVo
     * @return
     */
    @Update("UPDATE `blogroll`\n" +
            "SET `article_id` = #{articleId},\n" +
            " `order` = #{order}\n" +
            "WHERE\n" +
            "	(`id` = #{id})")
    int updateBanner(BannerVo bannerVo);

}
