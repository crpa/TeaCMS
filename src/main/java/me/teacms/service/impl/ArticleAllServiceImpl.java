package me.teacms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.result.JsonResult;
import me.teacms.dao.ArticleAllDao;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.entity.vo.SearchInfo;
import me.teacms.service.ArticleAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/24
 * Time: 17:28
 * Describe: 所有文章业务方法
 */
@Service
public class ArticleAllServiceImpl implements ArticleAllService {

    @Autowired
    private ArticleAllDao articleAllDao;


    /**
     * 查询当前文章的所有分类和标签
     * 操作当前对象
     * ArticleAllServiceImpl 业务类中使用
     * @return
     */
    private void findThisArticleCategoryTag(List<ArticleAllVo> articleAll) {

        for (ArticleAllVo item : articleAll) {
            item.setCategories(articleAllDao.findAllCategory(item.getId()));
            item.setTags(articleAllDao.findAllTag(item.getId()));
        }

        return;
    }

    /**
     * 查询所有文章信息
     * @param searchInfo 传入当前分页数 和 搜索信息
     * @return
     */
    @Override
    public JsonResult findArticleAll(SearchInfo searchInfo) {

        PageHelper.startPage(searchInfo.getPageId(), 10);
        List<ArticleAllVo> articleAll = articleAllDao.findArticleAll(searchInfo);

        findThisArticleCategoryTag(articleAll);

        PageInfo<ArticleAllVo> articleAllVoPageInfo = new PageInfo<>(articleAll);
        JsonResult jsonResult = new JsonResult();
        jsonResult.setSuccess(true);
        jsonResult.setObj(articleAllVoPageInfo);

        return jsonResult;
    }

    /**
     * 查询所有回收站文章 并 INNER JOIN 文章作者名
     * @param searchInfo 传入当前分页数 和 搜索信息
     * @return
     */
    @Override
    public JsonResult findAllTrashCanArticle(SearchInfo searchInfo) {

        PageHelper.startPage(searchInfo.getPageId(), 10);
        List<ArticleAllVo> allTrashCanArticle = articleAllDao.findAllTrashCanArticle(searchInfo);

        findThisArticleCategoryTag(allTrashCanArticle);

        PageInfo<ArticleAllVo> articleAllVoPageInfo = new PageInfo<>(allTrashCanArticle);
        JsonResult jsonResult = new JsonResult();
        jsonResult.setSuccess(true);
        jsonResult.setObj(articleAllVoPageInfo);

        return jsonResult;
    }

    /**
     * 逻辑删除文章
     * 更新文章状态
     * @param id    传入文章ID
     * @return
     */
    @Override
    public JsonResult deleteLogicArticle(Integer id) {
        JsonResult jsonResult = new JsonResult();

        int i = articleAllDao.updateLogicArticle(id);

        /**
         * 逻辑删除文章返回状态
         */
        if (i >= 0) {
            jsonResult.setSuccess(true);
            jsonResult.setObj(i);
        } else {
            jsonResult.setSuccess(false);
            jsonResult.setObj(i);
        }

        return jsonResult;
    }

    /**
     *  恢复文章到所有文章中
     * @param id    传入文章ID
     * @return
     */
    @Override
    public JsonResult updateArticleState(Integer id) {

        JsonResult jsonResult = new JsonResult();

        int i = articleAllDao.updateArticleState(id);

        /**
         * 恢复文章到所有文章中返回状态
         */
        if (i >= 0) {
            jsonResult.setSuccess(true);
            jsonResult.setObj(i);
        } else {
            jsonResult.setSuccess(false);
            jsonResult.setObj(i);
        }

        return jsonResult;
    }

    /**
     * 彻底删除文章
     * 需要调用 删除文章分类  删除文章标签
     * @param id    传入文章ID
     * @return
     */
    public JsonResult deleteArticle(Integer id) {

        int da = articleAllDao.deleteArticle(id);

        if (da >= 1) {
            int dac = articleAllDao.deleteArticleCategory(id);

            int dat = articleAllDao.deleteArticleTag(id);
        }
        /*返回状态*/
        JsonResult jsonResult = new JsonResult();
        jsonResult.setSuccess(true);
        jsonResult.setObj(da);

        return jsonResult;
    }

}
