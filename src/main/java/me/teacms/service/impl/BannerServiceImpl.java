package me.teacms.service.impl;

import me.teacms.common.result.JsonResult;
import me.teacms.dao.BannerDao;
import me.teacms.dao.BlogrollMapper;
import me.teacms.entity.vo.BannerVo;
import me.teacms.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/12
 * Time: 14:20
 * Describe: Banner Service
 */
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerDao bannerDao;
    @Autowired private BlogrollMapper blogrollMapper;

    /**
     * 查找所有Banner
     * @return
     */
    @Override
    public JsonResult findAllBanner() {
        JsonResult jsonResult = new JsonResult();

        List<BannerVo> allBanner = bannerDao.findAllBanner();

        jsonResult.setObj(allBanner);
        jsonResult.setSuccess(true);

        return jsonResult;
    }

    /**
     * Banner 查找要添加所有Banner的文章
     * @return
     */
    @Override
    public List<BannerVo> findAllBannerArticle() {

        List<BannerVo> allBannerArticle = bannerDao.findAllBannerArticle();

        return allBannerArticle;
    }

    /**
     * 删除Banner
     * @param id
     * @return
     */
    @Override
    public Integer deleteBanner(Integer id) {

        int i = blogrollMapper.deleteByPrimaryKey(id);

        return i;
    }

    /**
     * 添加Banner
     * @param bannerVo
     * @return
     */
    @Override
    public Integer addBanner(BannerVo bannerVo) {

        int i = bannerDao.addBanner(bannerVo);

        return i;
    }

    /**
     * 更新Banner
     * @param bannerVo
     * @return
     */
    @Override
    public Integer updateBanner(BannerVo bannerVo) {

        int i = bannerDao.updateBanner(bannerVo);

        return i;
    }
}
