package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.vo.ArticleNewVo;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 16:43
 * Describe: 添加文章 业务接口
 */
public interface ArticleNewService {

    /**
     * 添加文章
     * @param articleNewVo
     * @return
     */
    public JsonResult addArticle(ArticleNewVo articleNewVo);

}
