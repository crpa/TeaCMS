package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.User;
import me.teacms.entity.vo.UserVo;

import javax.servlet.http.HttpSession;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/1
 * Time: 23:50
 * Describe: 用户登陆-用户注销
 */
public interface AdminService {

    /**
     * 查询用户密码是否正确
     * @param userVo
     * @return
     */
    public User findUserPassIsCorrect(UserVo userVo);

    public JsonResult updateUserPass(HttpSession session, JsonResult jsonResult, User user, String psaa2);

}
