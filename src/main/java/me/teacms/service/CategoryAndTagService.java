package me.teacms.service;

import com.github.pagehelper.PageInfo;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/8
 * Time: 6:43
 * Describe: 文章分类和标签业务方法接口
 */
public interface CategoryAndTagService {

    /**
     * 查询所有分类文章
     * @param pageNum
     * @param alias
     * @return
     */
    public PageInfo findAllCategoryArticle(Integer pageNum, String alias);

    /**
     * 查询所有标签文章
     * @param pageNum
     * @param alias
     * @return
     */
    public PageInfo findAllTagArticle(Integer pageNum, String alias);

}
