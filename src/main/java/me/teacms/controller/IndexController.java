package me.teacms.controller;

import com.github.pagehelper.PageInfo;
import me.teacms.common.init.InitConfig;
import me.teacms.entity.WebConfig;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 1:28
 * Describe: 博客首页
 */
@Controller
public class IndexController {

    @Autowired
    private IndexService indexService;

    /**
     * 博客首页默认基础
     * @return
     */
    @RequestMapping(value = {"/", "/index.html"})
    public String index(ModelMap modelMap) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = indexService.findArticlePage(0);

        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/index";
    }

    @RequestMapping(value = "/so")
    public String findSo(ModelMap modelMap, String name) {
        WebConfig webConfig = InitConfig.getWebConfig();

        if (name.isEmpty()) {
            name ="";
        }
        List<ArticleAllVo> so = indexService.findSo(name);

        modelMap.put("articlePage", so);
        return "_themes/" + webConfig.getThemePath() + "/so";
    }

    /**
     * 博客文章分页
     * @param modelMap
     * @param pageNum
     * @return
     */
    @RequestMapping(value = "/page/{pageNum}")
    public String page(ModelMap modelMap, @PathVariable Integer pageNum) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = indexService.findArticlePage(pageNum);

        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/index";
    }

    /**
     * 获取文章内容
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}.html")
    public String content(ModelMap modelMap, @PathVariable Integer id) {
        WebConfig webConfig = InitConfig.getWebConfig();

        ArticleAllVo articleAllVo = indexService.findArticleById(id);

        modelMap.put("articleAllVo", articleAllVo);
        return "_themes/" + webConfig.getThemePath() + "/content";
    }

    @ResponseBody
    @RequestMapping(value = "/praise/updatearticlepraise")
    public Integer updateArticlePraise(@RequestBody Integer id) {

        Integer su = indexService.updateArticlePraise(id);

        return su;
    }

}
