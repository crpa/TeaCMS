package me.teacms.controller;

import com.github.pagehelper.PageInfo;
import me.teacms.common.init.InitConfig;
import me.teacms.entity.WebConfig;
import me.teacms.service.CategoryAndTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 1:32
 * Describe: 文章分类和标签Controller
 */
@Controller
public class CategoryAndTagController {

    @Autowired
    private CategoryAndTagService categoryAndTagService;

    /**
     * 查询所有分类文章
     * @param modelMap
     * @param alias
     * @return
     */
    @RequestMapping(value = "/category/{alias}")
    public String category(ModelMap modelMap, @PathVariable String alias) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = categoryAndTagService.findAllCategoryArticle(0, alias);

        modelMap.put("alias", alias);
        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/category";
    }

    /**
     * 分页
     * 查询所有分类文章
     * @param modelMap
     * @param alias
     * @return
     */
    @RequestMapping(value = "/category/{alias}/page/{pageNum}")
    public String categoryPage(ModelMap modelMap, @PathVariable String alias, @PathVariable Integer pageNum) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = categoryAndTagService.findAllCategoryArticle(pageNum, alias);

        modelMap.put("alias", alias);
        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/category";
    }

    /**
     * 查询所有标签文章
     * @param modelMap
     * @param alias
     * @return
     */
    @RequestMapping(value = "/tag/{alias}")
    public String tag(ModelMap modelMap, @PathVariable String alias) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = categoryAndTagService.findAllTagArticle(0, alias);

        modelMap.put("alias", alias);
        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/tag";
    }

    /**
     * 分页
     * 查询所有标签文章
     * @param modelMap
     * @param alias
     * @return
     */
    @RequestMapping(value = "/tag/{alias}/page/{pageNum}")
    public String tagPage(ModelMap modelMap, @PathVariable String alias, @PathVariable Integer pageNum) {
        WebConfig webConfig = InitConfig.getWebConfig();

        PageInfo articlePage = categoryAndTagService.findAllTagArticle(pageNum, alias);

        modelMap.put("alias", alias);
        modelMap.put("articlePage", articlePage);
        return "_themes/" + webConfig.getThemePath() + "/tag";
    }

}
